platform_check_image() {
	# i know no way to verify the image
	return 0;
}

platform_do_upgrade() {
	sync
	if [ "$SAVE_CONFIG" -ne 1 ]; then
		v "Performing factory reset ..."
		mtd erase user
	fi
	#write image to "ramdisk" partition
	get_image "$1" | mtd write - ramdisk
	sleep 1
}
