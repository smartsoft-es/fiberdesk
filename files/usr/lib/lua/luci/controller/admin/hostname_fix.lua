--[[
05/01/2016 hostname_fix by Pnia

Change hostname for network interfaces with dhcp protocol when updating system hostname

]]--

module("luci.controller.admin.hostname_fix", package.seeall)

function index()
	luci.sys.hostname = function(newname)
		if type(newname) == "string" and #newname > 0 then
			--change current hostname
			fs.writefile( "/proc/sys/kernel/hostname", newname )
			--change hostname for network interfaces with dhcp protocol
			local change = false
			local uci = require "luci.model.uci".cursor()
			uci:foreach("network", "interface", function(section)
				if section.proto == "dhcp" then
					uci:set("network", section[".name"], "hostname", newname)
					change = true;
				end
			end)
			if(change) then
				uci:commit("network");
			end
			return newname
		else
			return nixio.uname().nodename
		end
	end
end