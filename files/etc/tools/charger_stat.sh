echo "-------"
echo USB1 ATT `cat /sys/class/gpio/gpio115/value`
echo USB1 EN `cat /sys/class/gpio/gpio117/value`
echo "-------"
echo USB2 ATT `cat /sys/class/gpio/gpio112/value`
echo USB2 EN `cat /sys/class/gpio/gpio114/value`
echo "-------"
echo USB3 ATT `cat /sys/class/gpio/gpio123/value`
echo USB3 EN `cat /sys/class/gpio/gpio125/value`
echo "-------"
echo USB4 ATT `cat /sys/class/gpio/gpio120/value`
echo USB4 EN `cat /sys/class/gpio/gpio122/value`

