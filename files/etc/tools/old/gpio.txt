Los controladores de GPIOs tienen la siguiente numeración base:
gpio1: 224
gpio2: 192
gpio3: 160
gpio4: 128
expansor i2c EXT (0x70): 120
expansor i2c CPU (0x72): 112

La numeración de los gpios es:

- No probados:

GPIO2_12 (conector de expansion lateral): 204
GPIO2_13 (conector de expansion lateral): 205
GPIO2_14 (conector de expansion lateral): 206

- SFP
GPIO2_26 (SFP.TXFLT): 218
GPIO2_27 (SFP.TXOFF): 219
GPIO2_10 (SFP.LOSS): 202
GPIO2_11 (\SFP.DET): 203

- Carrier sw y frontal:

GPIO4_23 (POE.INT): 151
GPIO4_24 (POE.RST): 152

CPU_USB1_ATT: 112
CPU_USB1_FAULT: 113
CPU_USB1_EN: 114
CPU_USB2_ATT: 115
CPU_USB2_FAULT: 116
CPU_USB2_EN: 117
LED: 118
DFLT: 119

EXT_USB1_ATT: 120
EXT_USB1_FAULT: 121
EXT_USB1_EN: 122
EXT_USB2_ATT: 123
EXT_USB2_FAULT: 124
EXT_USB2_EN: 125
