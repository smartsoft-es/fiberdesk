# conector de expansion lateral
#GPIO2_12 (conector de expansion lateral): 204
#GPIO2_13 (conector de expansion lateral): 205
#GPIO2_14 (conector de expansion lateral): 206

#sfp
#GPIO2_26 (SFP.TXFLT): 218
#GPIO2_27 (SFP.TXOFF): 219
#GPIO2_10 (SFP.LOSS): 202
#GPIO2_11 (\SFP.DET): 203

# Carrier sw y frontal:
#GPIO4_24 (POE.RST): 152
#CPU_USB1_ATT: 112
#CPU_USB1_FAULT: 113
#CPU_USB1_EN: 114
#CPU_USB2_ATT: 115
#CPU_USB2_FAULT: 116
#CPU_USB2_EN: 117
#LED: 118
#DFLT: 119
#EXT_USB1_ATT: 120
#EXT_USB1_FAULT: 121
#EXT_USB1_EN: 122
#EXT_USB2_ATT: 123
#EXT_USB2_FAULT: 124
#EXT_USB2_EN: 125

#expansion lateral
echo 204 > /sys/class/gpio/export
echo 205 > /sys/class/gpio/export
echo 206 > /sys/class/gpio/export

#sfp
echo 218 > /sys/class/gpio/export
echo 219 > /sys/class/gpio/export
echo 202 > /sys/class/gpio/export
echo 203 > /sys/class/gpio/export

echo out > /sys/class/gpio/gpio219/direction
echo 0 > /sys/class/gpio/gpio219/value

# Carrier sw y frontal:
#echo 152 > /sys/class/gpio/export
#echo out > /sys/class/gpio/gpio152/direction
#echo 1 > /sys/class/gpio/gpio152/value

echo 112 > /sys/class/gpio/export
echo 113 > /sys/class/gpio/export
echo 114 > /sys/class/gpio/export
echo 115 > /sys/class/gpio/export
echo 116 > /sys/class/gpio/export
echo 117 > /sys/class/gpio/export
echo 118 > /sys/class/gpio/export
echo 119 > /sys/class/gpio/export

echo out > /sys/class/gpio/gpio114/direction
echo out > /sys/class/gpio/gpio117/direction
echo out > /sys/class/gpio/gpio118/direction

echo 1 > /sys/class/gpio/gpio114/value
echo 1 > /sys/class/gpio/gpio117/value
echo 1 > /sys/class/gpio/gpio118/value

echo 120 > /sys/class/gpio/export
echo 121 > /sys/class/gpio/export
echo 122 > /sys/class/gpio/export
echo 123 > /sys/class/gpio/export
echo 124 > /sys/class/gpio/export
echo 125 > /sys/class/gpio/export

echo out > /sys/class/gpio/gpio122/direction
echo out > /sys/class/gpio/gpio125/direction

echo 1 > /sys/class/gpio/gpio122/value
echo 1 > /sys/class/gpio/gpio125/value
